import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { RestProvider } from "../../providers/rest.provider";

/**
 * Generated class for the VehicleViewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: "vehicle-view",
  segment: "vehicle-view/:car_id"
})
@Component({
  selector: "page-vehicle-view",
  templateUrl: "vehicle-view.html"
})
export class VehicleViewPage {
  car_id: string;
  car: any;
  mode: string = "reparation";
  reparations: any[];
  inspections: any[];
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private rest: RestProvider
  ) {
    this.car_id = this.navParams.get("car_id");
  }

  ionViewWillEnter() {
    this.rest.getCar(this.car_id).subscribe(car => {
      this.car = car;
    });

    if (this.mode == "reparation") {
      this.getReparations();
    } else if (this.mode == "inspection") {
      this.getInspections();
    }
  }

  getReparations() {
    this.rest.getReparations(this.car_id).subscribe((reparations: any[]) => {
      this.reparations = reparations;
    });
  }

  getInspections() {
    this.rest.getInspections(this.car_id).subscribe((inspections: any[]) => {
      this.inspections = inspections;
    });
  }
}

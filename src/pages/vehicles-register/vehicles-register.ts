import { Component } from "@angular/core";
import { IonicPage, NavController, NavParams } from "ionic-angular";
import { RestProvider } from "../../providers/rest.provider";

/**
 * Generated class for the VehiclesRegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: "vehicle-register",
  segment: "vehicle-register"
})
@Component({
  selector: "page-vehicles-register",
  templateUrl: "vehicles-register.html"
})
export class VehiclesRegisterPage {
  newCar: any = {};
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private rest: RestProvider
  ) {}

  ionViewDidLoad() {
    console.log("ionViewDidLoad VehiclesRegisterPage");
  }

  sendData() {
    this.rest.addCar(this.newCar).subscribe(() => this.navCtrl.pop());
  }
}

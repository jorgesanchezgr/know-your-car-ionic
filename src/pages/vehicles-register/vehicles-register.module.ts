import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VehiclesRegisterPage } from './vehicles-register';

@NgModule({
  declarations: [
    VehiclesRegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(VehiclesRegisterPage),
  ],
})
export class VehiclesRegisterPageModule {}

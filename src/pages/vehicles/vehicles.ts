import { Component } from "@angular/core";
import { IonicPage, NavController } from "ionic-angular";
import { RestProvider } from "../../providers/rest.provider";

/**
 * Generated class for the VehiclesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: "vehicles",
  segment: "vehicles"
})
@Component({
  selector: "page-vehicles",
  templateUrl: "vehicles.html"
})
export class VehiclesPage {
  protected cars: any[];

  constructor(public navCtrl: NavController, private rest: RestProvider) {}

  ionViewWillEnter() {
    this.rest.getCars().subscribe((cars: any[]) => (this.cars = cars));
  }

  registerNewCar() {
    this.navCtrl.push("vehicle-register");
  }

  view(car) {
    this.navCtrl.push("vehicle-view", { car_id: car._id });
  }
}

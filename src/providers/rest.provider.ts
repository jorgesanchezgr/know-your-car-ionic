import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class RestProvider {
  basePath = "http://192.168.0.104:8080/api";

  constructor(private http: HttpClient) {}

  getCars() {
    return this.http.get(this.basePath + "/cars");
  }

  getCar(id) {
    return this.http.get(this.basePath + "/cars/" + id);
  }

  getReparations(id) {
    return this.http.get(this.basePath + "/cars/" + id + "/reparations");
  }
  getInspections(id) {
    return this.http.get(this.basePath + "/cars/" + id + "/inspections");
  }

  addCar(car) {
    return this.http.post(this.basePath + "/cars", car);
  }
}
